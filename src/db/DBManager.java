package db;
import java.sql.*;

public class DBManager {
	private static final String DB_DRIVER = "org.h2.Driver";
	//private static final String DB_BASE_URL = "jdbc:h2:/Users/hax/eclipse-workspace/HomeBanking/src/db;AUTO_SERVER=TRUE";
	private static final String DB_BASE_URL = "jdbc:h2:/tmp/db;AUTO_SERVER=TRUE";
	private static final String DB_USERNAME = "sa";
	private static final String DB_PASSWORD = "";
	
	public static Connection connect(Boolean setAutoCommit) {
		Connection c = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
		try {
			c = DriverManager.getConnection(DB_BASE_URL, DB_USERNAME, DB_PASSWORD);
			c.setAutoCommit(setAutoCommit);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		

		return c;
	}
}
