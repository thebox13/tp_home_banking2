package db;

public class DBColumn {
	private String nombre;
	private String tipo;
	
	public DBColumn (String nombre, String tipo) {
		this.setNombre(nombre);
		this.setTipo(tipo);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
