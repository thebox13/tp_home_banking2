package db;
import java.sql.*;
import java.util.List;

import db.DBManager;
import exceptions.DefinicionDatosException;


public class TableManager {
	public void crearTabla(String nombreTabla, List<DBColumn> columns) throws DefinicionDatosException {
		Connection c = DBManager.connect(false);

		StringBuilder sqlString = new StringBuilder();
		sqlString.append("CREATE TABLE ");
		sqlString.append(nombreTabla);
		sqlString.append(" (Id BIGINT GENERATED ALWAYS AS IDENTITY PRIMARY KEY, ");

		for (int i = 0; i < columns.size(); i++) {
			sqlString.append(columns.get(i).getNombre());
			sqlString.append(" ");
			sqlString.append(columns.get(i).getTipo());
			
			if(i != columns.size() - 1) {
				sqlString.append(", ");
			}
		}

		sqlString.append(")");

		try {
			Statement s = c.createStatement();
			s.execute(sqlString.toString());
		} catch (SQLException e) {
			try {
				c.rollback();
				e.printStackTrace();
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw new DefinicionDatosException(e1.getMessage(), e1.getCause());
			}
		} finally {
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DefinicionDatosException(e.getMessage(), e.getCause());
			}
		}

	}
	

	public void eliminarTabla(String tableName) throws DefinicionDatosException {

		Connection c = DBManager.connect(false);

		StringBuilder dropSql = new StringBuilder();
		dropSql.append("DROP TABLE IF EXISTS ");
		dropSql.append(tableName);
		
		try {
			Statement s = c.createStatement();
			s.execute(dropSql.toString());
			c.commit();
		} catch (SQLException e) {
			try {
				c.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				throw new DefinicionDatosException(e1.getMessage(), e1.getCause());
			}
		} finally {
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw new DefinicionDatosException(e.getMessage(), e.getCause());
			}
		}

	}

}
