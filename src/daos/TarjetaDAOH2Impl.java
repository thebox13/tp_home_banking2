package daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import data.Cliente;
import data.Cuenta;
import data.Tarjeta;
import db.DBManager;
import exceptions.AccesoDatosException;

public class TarjetaDAOH2Impl implements TarjetaDAO {

	@Override
	public void crear(Tarjeta tarjeta) throws AccesoDatosException {
		String usuario = tarjeta.getUsuario();
		int nroTarjeta = tarjeta.getNroTarjeta();
		float consumo = tarjeta.getConsumo();
		float limite = tarjeta.getLimite();
		
		Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            String sql = "INSERT INTO Tarjetas (NombreUsuario, NumeroTarjeta, Consumo, Limite) VALUES ('" + usuario + "', " + nroTarjeta + ", " + 
            consumo + ", " + limite + ")";
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public void actualizar(Tarjeta tarjeta) throws AccesoDatosException {
		String usuario = tarjeta.getUsuario();
		int nroTarjeta = tarjeta.getNroTarjeta();
		float consumo = tarjeta.getConsumo();
		float limite = tarjeta.getLimite();

        String sql = "UPDATE Tarjetas set NumeroTarjeta = " + nroTarjeta + ", Consumo = " + consumo + ", Limite = " + limite + 
        		" WHERE NombreUsuario = '" + usuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public void borrar(int nroTarjeta) throws AccesoDatosException {
		String sql = "DELETE FROM Tarjetas WHERE NumeroTarjeta = " + nroTarjeta;
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public List<Tarjeta> obtenerPorNombre(String nombreUsuario) throws AccesoDatosException {
		List<Tarjeta> result = new ArrayList<>();
		String sql = "SELECT * FROM Tarjetas WHERE NombreUsuario = '" + nombreUsuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            while (rs.next()) {
            	String rsNombreCliente = rs.getString("NombreUsuario");
            	int nroTarjeta = rs.getInt("NumeroTarjeta");
            	float consumo = rs.getFloat("Consumo");
            	float limite = rs.getFloat("Limite");
                
            	Tarjeta tarjeta = new Tarjeta(rsNombreCliente, nroTarjeta, consumo, limite);
                result.add(tarjeta);
            }
        } catch (SQLException e) {
            try {
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return result;
	}
}
