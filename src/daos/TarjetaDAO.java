package daos;

import java.util.List;

import data.Tarjeta;
import exceptions.AccesoDatosException;

public interface TarjetaDAO {
	void crear(Tarjeta tarjeta) throws AccesoDatosException;
	void actualizar(Tarjeta tarjeta) throws AccesoDatosException;
	void borrar(int nroTarjeta) throws AccesoDatosException;
	List<Tarjeta> obtenerPorNombre(String nombreUsuario) throws AccesoDatosException;

}
