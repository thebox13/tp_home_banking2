package daos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import data.Cliente;
import data.Cuenta;
import data.Tarjeta;
import db.DBManager;
import exceptions.AccesoDatosException;

public class CuentaDAOH2Impl implements CuentaDAO {
	@Override
	public void crear(Cuenta cuenta) throws AccesoDatosException {
		String usuario = cuenta.getUsuario();
		String tipo_cuenta = cuenta.getTipo_cuenta();
		int nro_cuenta = cuenta.getNro_cuenta();
		float dinero = cuenta.getDinero();
		
		Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            String sql = "INSERT INTO Cuentas (NombreUsuario, TipoCuenta, NumeroCuenta, Dinero) VALUES ('" + usuario + "', '" + tipo_cuenta + "', " + 
            nro_cuenta + ", " + dinero + ")";
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public void borrar(String nombreUsuario) throws AccesoDatosException {
		String sql = "DELETE FROM Cuentas WHERE NombreUsuario = '" + nombreUsuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	
	@Override
	public void borrar(int nroCuenta) throws AccesoDatosException {
		String sql = "DELETE FROM Cuentas WHERE NumeroCuenta = " + nroCuenta;
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public List<Cuenta> obtenerPorNombre(String nombreUsuario) throws AccesoDatosException {
		List<Cuenta> result = new ArrayList<>();
		String sql = "SELECT * FROM Cuentas WHERE NombreUsuario = '" + nombreUsuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            
            while (rs.next()) {
            	String usuario = rs.getString("NombreUsuario");
        		String tipo_cuenta = rs.getString("TipoCuenta");
        	    int nro_cuenta = rs.getInt("NumeroCuenta");
        	    float dinero = rs.getFloat("Dinero");
                
        	    Cuenta cuenta = new Cuenta(usuario, tipo_cuenta, nro_cuenta, dinero);
                result.add(cuenta);
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return result;
	}

	@Override
	public int ultimaCuenta() throws AccesoDatosException {
		String sql = "SELECT MAX(NUMEROCUENTA) FROM CUENTAS";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            if (rs.next()) {
            	int ultimaCuenta = rs.getInt("MAX(NUMEROCUENTA)");
                
                return ultimaCuenta;
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return 0;
	}

	@Override
	public List<Cuenta> listarTodo() throws AccesoDatosException {
		List<Cuenta> result = new ArrayList<>();
		String sql = "SELECT * FROM Cuentas";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);
            
            while (rs.next()) {
            	String usuario = rs.getString("NombreUsuario");
        		String tipo_cuenta = rs.getString("TipoCuenta");
        	    int nro_cuenta = rs.getInt("NumeroCuenta");
        	    float dinero = rs.getFloat("Dinero");
                
        	    Cuenta cuenta = new Cuenta(usuario, tipo_cuenta, nro_cuenta, dinero);
                result.add(cuenta);
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return result;
	}
	
	public void actualizar(Cuenta cuenta) throws AccesoDatosException {
		String usuario = cuenta.getUsuario();
		String tipoCuenta = cuenta.getTipo_cuenta();
		int nroCuenta = cuenta.getNro_cuenta();
		float dinero = cuenta.getDinero();
		
        String sql = "UPDATE Cuentas set NombreUsuario = '" + usuario + "', TipoCuenta = '" + tipoCuenta + "', NumeroCuenta = " + nroCuenta +
        		", Dinero = " + dinero + " WHERE NumeroCuenta = '" + nroCuenta + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	

	public boolean verificarCuenta(String nombreUsuario, int nroCuenta) throws AccesoDatosException {
		String sql = "SELECT * FROM Cuentas WHERE NombreUsuario = '" + nombreUsuario + "' AND NumeroCuenta = " + nroCuenta;
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            if (rs.next()) {
                return true;
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return false;
	}
	
	public Cuenta obtenerPorCuenta(int nroCuenta ) throws AccesoDatosException {
		String sql = "SELECT * FROM Cuentas WHERE NumeroCuenta = " + nroCuenta;
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            if (rs.next()) {
            	String usuario = rs.getString("NombreUsuario");
        		String tipo_cuenta = rs.getString("TipoCuenta");
        	    int nro_cuenta = rs.getInt("NumeroCuenta");
        	    float dinero = rs.getFloat("Dinero");
                
        	    Cuenta cuenta = new Cuenta(usuario, tipo_cuenta, nro_cuenta, dinero);
        	    return cuenta;
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return null;
	}
}
