package daos;

import java.util.List;

import data.Cuenta;
import data.Movimiento;
import exceptions.AccesoDatosException;

public interface MovimientoDAO {
	void crear(Movimiento movimiento) throws AccesoDatosException;
	void borrar(int nroCuenta) throws AccesoDatosException;
	List<Movimiento> obtenerPorNroCuenta(int nroCuenta) throws AccesoDatosException;
}
