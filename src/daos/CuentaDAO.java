package daos;

import java.util.List;

import data.Cuenta;
import exceptions.AccesoDatosException;

public interface CuentaDAO {
	void crear(Cuenta cuenta) throws AccesoDatosException;
	void borrar(String nombreUsuario) throws AccesoDatosException;
	void borrar(int nroCuenta) throws AccesoDatosException;
	int ultimaCuenta() throws AccesoDatosException;
	void actualizar(Cuenta cuenta) throws AccesoDatosException;
	boolean verificarCuenta(String nombreUsuario, int nroCuenta) throws AccesoDatosException;
	Cuenta obtenerPorCuenta(int nroCuenta ) throws AccesoDatosException;
	
	List<Cuenta> obtenerPorNombre(String nombreUsuario)throws AccesoDatosException ;
	List<Cuenta> listarTodo()throws AccesoDatosException ;
}
