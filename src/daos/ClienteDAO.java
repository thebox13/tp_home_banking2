package daos;

import java.util.List;
import data.Cliente;
import exceptions.AccesoDatosException;

public interface ClienteDAO {
	void crear(Cliente cliente) throws AccesoDatosException;
	void borrar(String nombreCliente) throws AccesoDatosException;
	void actualizar(Cliente cliente) throws AccesoDatosException;
	
	Cliente obtenerPorNombre(String nombreCliente) throws AccesoDatosException;
	
	List<Cliente> obtenerTodo() throws AccesoDatosException;
}
