package daos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import data.Cliente;
import data.Movimiento;
import db.DBManager;
import exceptions.AccesoDatosException;

public class MovimientoDAOH2Impl implements MovimientoDAO {

	@Override
	public void crear(Movimiento movimiento) throws AccesoDatosException {
		int nro_cuenta = movimiento.getNro_cuenta();
		float movimiento_dinero = movimiento.getMovimientoDinero();
        
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            String sql = "INSERT INTO Movimientos (NumeroCuenta, MovimientoDinero) VALUES (" + nro_cuenta + ", " + movimiento_dinero + ")";
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	
	@Override
	public void borrar(int nroCuenta) throws AccesoDatosException {
		String sql = "DELETE FROM Movimientos WHERE NumeroCuenta = '" + nroCuenta + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}

	@Override
	public List<Movimiento> obtenerPorNroCuenta(int nroCuenta) throws AccesoDatosException {
		List<Movimiento> result = new ArrayList<>();
        String sql = "SELECT * FROM Movimientos WHERE " + nroCuenta + " = NumeroCuenta";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            while (rs.next()) {
            	int nro_cuenta = rs.getInt("NumeroCuenta");
        		float movimiento_dinero = rs.getFloat("MovimientoDinero");
                
                Movimiento movimiento = new Movimiento(nro_cuenta, movimiento_dinero);
                result.add(movimiento);
            }
        } catch (SQLException e) {
            try {
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return result;
	}
}
