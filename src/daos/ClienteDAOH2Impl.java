package daos;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import data.Cliente;
import db.DBManager;
import exceptions.AccesoDatosException;

public class ClienteDAOH2Impl implements ClienteDAO {
	@Override
	public void crear(Cliente cliente) throws AccesoDatosException {
		String usuario = cliente.getNombreUsuario();
        String email = cliente.getEmail();
        String password = cliente.getPassword();
        String nombreFull = cliente.getNombreFull();
        String dni = cliente.getDni();
        String numeroContacto = cliente.getNumeroContacto();
        
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            String sql = "INSERT INTO Clientes (NombreUsuario, Email, Password, NombreFull, DNI, NumeroContacto) VALUES ('" + usuario + "', '" + email + "', '" + 
            password + "', '" + nombreFull + "', '" + dni + "', '" + numeroContacto + "')";
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                e.printStackTrace();
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	
	@Override
	public void borrar(String nombreCliente) throws AccesoDatosException {
		String sql = "DELETE FROM Clientes WHERE NombreUsuario = '" + nombreCliente + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	
	@Override
	public void actualizar(Cliente cliente) throws AccesoDatosException {
		String usuario = cliente.getNombreUsuario();
        String email = cliente.getEmail();
        String password = cliente.getPassword();
        String nombreFull = cliente.getNombreFull();
        String dni = cliente.getDni();
        String numeroContacto = cliente.getNumeroContacto();

        String sql = "UPDATE Clientes set Email = '" + email + "', Password = '" + password + "', NombreFull = '" + nombreFull + "', DNI = '" + dni +
        		"', NumeroContacto = '" + numeroContacto + "' WHERE NombreUsuario = '" + usuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            s.executeUpdate(sql);
            c.commit();
        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
	}
	
	@Override
	public Cliente obtenerPorNombre(String nombreUsuario) throws AccesoDatosException {
		String sql = "SELECT * FROM Clientes WHERE NombreUsuario = '" + nombreUsuario + "'";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            if (rs.next()) {
            	String rsNombreCliente = rs.getString("NombreUsuario");
                String nombreFull = rs.getString("NombreFull");
                String email = rs.getString("Email");
                String password = rs.getString("Password");
                String dni = rs.getString("DNI");
                String numeroContacto = rs.getString("NumeroContacto");
                
                Cliente cliente = new Cliente(rsNombreCliente, nombreFull, email, password, dni, numeroContacto);
                return cliente;
            }

        } catch (SQLException e) {
            try {
                c.rollback();
                e.printStackTrace();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return null;
	}
	
	@Override
	public List<Cliente> obtenerTodo() throws AccesoDatosException {
		List<Cliente> result = new ArrayList<>();
        String sql = "SELECT * FROM Clientes";
        Connection c = DBManager.connect(false);
        try {
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(sql);

            
            while (rs.next()) {
                String rsNombreCliente = rs.getString("NombreUsuario");
                String nombreFull = rs.getString("NombreFull");
                String email = rs.getString("Email");
                String password = rs.getString("Password");
                String dni = rs.getString("DNI");
                String numeroContacto = rs.getString("NumeroContacto");
                
                Cliente cliente = new Cliente(rsNombreCliente, nombreFull, email, password, dni, numeroContacto);
                result.add(cliente);
            }
        } catch (SQLException e) {
            try {
                c.rollback();
                
                throw new AccesoDatosException(e.getMessage(), e.getCause());
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        } finally {
            try {
                c.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
                
                throw new AccesoDatosException(e1.getMessage(), e1.getCause());
            }
        }
        return result;
	}

}