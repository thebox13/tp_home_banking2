package exceptions;

public class FormatoEmailException extends TextoInvalidoException {
	private static final long serialVersionUID = 1L;

	public FormatoEmailException() {
		super();
	}
	
	public FormatoEmailException(String message) {
		super(message);
	}
	
	public FormatoEmailException(Throwable cause) {
		super(cause);
	}
	
	public FormatoEmailException(String message, Throwable cause) {
		super(message, cause);
	}
}
