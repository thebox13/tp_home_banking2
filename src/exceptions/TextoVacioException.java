package exceptions;

public class TextoVacioException extends TextoInvalidoException {

	private static final long serialVersionUID = 1L;

	public TextoVacioException() {
		super();
	}
	
	public TextoVacioException(String message) {
		super(message);
	}
	
	public TextoVacioException(Throwable cause) {
		super(cause);
	}
	
	public TextoVacioException(String message, Throwable cause) {
		super(message, cause);
	}
}
