package exceptions;

public class DefinicionDatosException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DefinicionDatosException() {
		super();
	}
	
	public DefinicionDatosException(String message) {
		super(message);
	}
	
	public DefinicionDatosException(Throwable cause) {
		super(cause);
	}
	
	public DefinicionDatosException(String message, Throwable cause) {
		super(message, cause);
	}

}