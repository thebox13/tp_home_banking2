package exceptions;

public class AccesoDatosException extends Exception {

	private static final long serialVersionUID = 1L;

	public AccesoDatosException() {
		super();
	}
	
	public AccesoDatosException(String message) {
		super(message);
	}
	
	public AccesoDatosException(Throwable cause) {
		super(cause);
	}
	
	public AccesoDatosException(String message, Throwable cause) {
		super(message, cause);
	}
}
