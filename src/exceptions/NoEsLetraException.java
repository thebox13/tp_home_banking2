package exceptions;

public class NoEsLetraException extends TextoInvalidoException {
	private static final long serialVersionUID = 1L;

	public NoEsLetraException() {
		super();
	}

	public NoEsLetraException(String message) {
		super(message);
	}

	public NoEsLetraException(Throwable cause) {
		super(cause);
	}

	public NoEsLetraException(String message, Throwable cause) {
		super(message, cause);
	}
}
