package exceptions;

public class NumeroInvalidoException extends ServicioException {
	private static final long serialVersionUID = 1L;

	public NumeroInvalidoException() {
		super();
	}
	
	public NumeroInvalidoException(String message) {
		super(message);
	}
	
	public NumeroInvalidoException(Throwable cause) {
		super(cause);
	}
	
	public NumeroInvalidoException(String message, Throwable cause) {
		super(message, cause);
	}
}
