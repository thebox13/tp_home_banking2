package exceptions;

public class NoEsNumeroException extends TextoInvalidoException {
	private static final long serialVersionUID = 1L;

	public NoEsNumeroException() {
		super();
	}

	public NoEsNumeroException(String message) {
		super(message);
	}

	public NoEsNumeroException(Throwable cause) {
		super(cause);
	}

	public NoEsNumeroException(String message, Throwable cause) {
		super(message, cause);
	}
}
