package exceptions;

public class LargoInvalidoException extends TextoInvalidoException {

	private static final long serialVersionUID = 1L;

	public LargoInvalidoException() {
		super();
	}
	
	public LargoInvalidoException(String message) {
		super(message);
	}
	
	public LargoInvalidoException(Throwable cause) {
		super(cause);
	}
	
	public LargoInvalidoException(String message, Throwable cause) {
		super(message, cause);
	}
}
