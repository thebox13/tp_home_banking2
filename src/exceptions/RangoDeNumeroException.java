package exceptions;

public class RangoDeNumeroException extends NumeroInvalidoException {
	private static final long serialVersionUID = 1L;

	public RangoDeNumeroException() {
		super();
	}

	public RangoDeNumeroException(String message) {
		super(message);
	}

	public RangoDeNumeroException(Throwable cause) {
		super(cause);
	}

	public RangoDeNumeroException(String message, Throwable cause) {
		super(message, cause);
	}
}
