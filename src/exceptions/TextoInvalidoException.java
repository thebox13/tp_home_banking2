package exceptions;

public class TextoInvalidoException extends ServicioException {
	private static final long serialVersionUID = 1L;

	public TextoInvalidoException() {
		super();
	}
	
	public TextoInvalidoException(String message) {
		super(message);
	}
	
	public TextoInvalidoException(Throwable cause) {
		super(cause);
	}
	
	public TextoInvalidoException(String message, Throwable cause) {
		super(message, cause);
	}
}
