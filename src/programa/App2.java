package programa;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controlador.ControladorAdminPanelManager;

import java.util.ArrayList;
import java.util.List;
import data.*;
import db.TableManager;
import db.DBColumn;
import exceptions.*;
import servicio.ClienteService;
import servicio.CuentaService;
import servicio.MovimientoService;
import servicio.TarjetaService;

public class App2 {
	public static void main(String[] args) throws DefinicionDatosException, ServicioException {
		JFrame frame = new JFrame("Home Banking");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		
		ControladorAdminPanelManager panelManager = null;
		
		TableManager tm = new TableManager();
		List<DBColumn> columnas_clientes = new ArrayList<DBColumn>();
		columnas_clientes.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("NombreFull", "VARCHAR(512)"));
		columnas_clientes.add(new DBColumn("Email", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("Password", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("DNI", "VARCHAR(128)"));
		columnas_clientes.add(new DBColumn("NumeroContacto", "VARCHAR(256)"));
		
		List<DBColumn> columnas_cuentas = new ArrayList<DBColumn>();
		columnas_cuentas.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_cuentas.add(new DBColumn("TipoCuenta", "VARCHAR(256)"));
		columnas_cuentas.add(new DBColumn("NumeroCuenta", "INT"));
		columnas_cuentas.add(new DBColumn("Dinero", "DEC(20,2)"));

		List<DBColumn> columnas_movimientos = new ArrayList<DBColumn>();
		columnas_movimientos.add(new DBColumn("NumeroCuenta", "INT"));
		columnas_movimientos.add(new DBColumn("MovimientoDinero", "DEC(20,2)"));
		
		List<DBColumn> columnas_tarjetas = new ArrayList<DBColumn>();
		columnas_tarjetas.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_tarjetas.add(new DBColumn("NumeroTarjeta", "INT"));
		columnas_tarjetas.add(new DBColumn("Consumo", "DEC(20,2)"));
		columnas_tarjetas.add(new DBColumn("Limite", "DEC(20,2)"));
		
		try {
			// Creamos el cliente "administrador" que sera el empleado del banco
			tm.eliminarTabla("Clientes");
			tm.crearTabla("Clientes", columnas_clientes);
			ClienteService userDao = new ClienteService();
			Cliente user1 = new Cliente("administrador", "Leandro Villatoro", "leandro@villatoro.com", "12345", "34614066", "44519999");
			userDao.create(user1);
			
			// Creamos la tabla Cuentas
			tm.eliminarTabla("Cuentas");
			tm.crearTabla("Cuentas", columnas_cuentas);
			CuentaService cuentaDao = new CuentaService();
			Cuenta cuenta1 = new Cuenta("administrador", "CA", 100200, 50000);
			cuentaDao.create(cuenta1);
			
			// Creamos la tabla Movmimientos
			tm.eliminarTabla("Movimientos");
			tm.crearTabla("Movimientos", columnas_movimientos);
			MovimientoService movimientoDao = new MovimientoService();
			Movimiento movimiento1 = new Movimiento(100200, -500);
			movimientoDao.create(movimiento1);
			
			// Creamos la tabla Tarjetas
			tm.eliminarTabla("Tarjetas");
			tm.crearTabla("Tarjetas", columnas_tarjetas);
			TarjetaService tarjetaDao = new TarjetaService();
			Tarjeta tarjeta1 = new Tarjeta("administrador", 1111111, 20000, 100000);
			tarjetaDao.create(tarjeta1);
			
			panelManager = new ControladorAdminPanelManager();
			panelManager.buildManager();
			panelManager.mostrarHomePanel();
			panelManager.mostrarFrame();
			
		} catch (DefinicionDatosException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(frame, "Error cargando las tablas.", "Error", JOptionPane.ERROR_MESSAGE);
			try {
				tm.eliminarTabla("Clientes");
				tm.eliminarTabla("Cuentas");
				tm.eliminarTabla("Movimientos");
				tm.eliminarTabla("Tarjetas");
			} catch (DefinicionDatosException e) {
				e.printStackTrace();
			}
		} catch (ServicioException e2) {
			e2.printStackTrace();
			tm.eliminarTabla("Clientes");
			tm.eliminarTabla("Cuentas");
			tm.eliminarTabla("Movimientos");
			tm.eliminarTabla("Tarjetas");
		}
	}
}