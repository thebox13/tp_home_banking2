package programa;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controlador.ControladorLogin;
import controlador.ControladorAdminPanelManager;

import java.util.ArrayList;
import java.util.List;
import data.*;
import db.TableManager;
import db.DBColumn;
import exceptions.*;
import servicio.ClienteService;
import servicio.CuentaService;
import servicio.MovimientoService;
import servicio.TarjetaService;
import vista.PantallaLogin;

public class App {
	public static void main(String[] args) throws DefinicionDatosException, ServicioException {
		
		TableManager tm = new TableManager();
		List<DBColumn> columnas_clientes = new ArrayList<DBColumn>();
		columnas_clientes.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("NombreFull", "VARCHAR(512)"));
		columnas_clientes.add(new DBColumn("Email", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("Password", "VARCHAR(256)"));
		columnas_clientes.add(new DBColumn("DNI", "VARCHAR(128)"));
		columnas_clientes.add(new DBColumn("NumeroContacto", "VARCHAR(256)"));
		
		List<DBColumn> columnas_cuentas = new ArrayList<DBColumn>();
		columnas_cuentas.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_cuentas.add(new DBColumn("TipoCuenta", "VARCHAR(256)"));
		columnas_cuentas.add(new DBColumn("NumeroCuenta", "INT"));
		columnas_cuentas.add(new DBColumn("Dinero", "DEC(20,2)"));

		List<DBColumn> columnas_movimientos = new ArrayList<DBColumn>();
		columnas_movimientos.add(new DBColumn("NumeroCuenta", "INT"));
		columnas_movimientos.add(new DBColumn("MovimientoDinero", "DEC(20,2)"));
		
		List<DBColumn> columnas_tarjetas = new ArrayList<DBColumn>();
		columnas_tarjetas.add(new DBColumn("NombreUsuario", "VARCHAR(256)"));
		columnas_tarjetas.add(new DBColumn("NumeroTarjeta", "INT"));
		columnas_tarjetas.add(new DBColumn("Consumo", "DEC(20,2)"));
		columnas_tarjetas.add(new DBColumn("Limite", "DEC(20,2)"));
		
		try {
			// Creamos el cliente "administrador" que sera el empleado del banco
			tm.eliminarTabla("Clientes");
			tm.crearTabla("Clientes", columnas_clientes);
			ClienteService userDao = new ClienteService();
			Cliente user1 = new Cliente("administrador", "Leandro Villatoro", "leandro@villatoro.com", "12345", "34614066", "44519999");
			Cliente user2 = new Cliente("jjose", "Juan Jose", "juan@jose.com", "12345", "312314066", "44519119");
			userDao.create(user1);
			userDao.create(user2);
			
			// Creamos la tabla Cuentas
			tm.eliminarTabla("Cuentas");
			tm.crearTabla("Cuentas", columnas_cuentas);
			CuentaService cuentaDao = new CuentaService();
			Cuenta cuenta1 = new Cuenta("administrador", "CA", 111111, 142134);
			Cuenta cuenta2 = new Cuenta("administrador", "CA", 888123, 123123);
			Cuenta cuenta3 = new Cuenta("administrador", "CA", 135121, 234552);
			Cuenta cuenta4 = new Cuenta("administrador", "CA", 128123, 111111);
			Cuenta cuenta5 = new Cuenta("jjose", "CA", 123113, 31111);
			Cuenta cuenta6 = new Cuenta("jjose", "CC", 432113, 413131);
			Cuenta cuenta7 = new Cuenta("jjose", "CAD", 515145, 13315);
			Cuenta cuenta8 = new Cuenta("jjose", "CAP", 235112, 123565);
			cuentaDao.create(cuenta1);
			cuentaDao.create(cuenta2);
			cuentaDao.create(cuenta3);
			cuentaDao.create(cuenta4);
			cuentaDao.create(cuenta5);
			cuentaDao.create(cuenta6);
			cuentaDao.create(cuenta7);
			cuentaDao.create(cuenta8);
			
			// Creamos la tabla Movmimientos
			tm.eliminarTabla("Movimientos");
			tm.crearTabla("Movimientos", columnas_movimientos);
			MovimientoService movimientoDao = new MovimientoService();
			Movimiento movimiento1 = new Movimiento(100200, -500);
			movimientoDao.create(movimiento1);
			
			// Creamos la tabla Tarjetas
			tm.eliminarTabla("Tarjetas");
			tm.crearTabla("Tarjetas", columnas_tarjetas);
			TarjetaService tarjetaDao = new TarjetaService();
			Tarjeta tarjeta1 = new Tarjeta("administrador", 1111111, 20000, 100000);
			tarjetaDao.create(tarjeta1);
			
			ControladorLogin cLogin = new ControladorLogin();
			cLogin.mostrar();
			
		} catch (DefinicionDatosException e1) {
			e1.printStackTrace();
			//JOptionPane.showMessageDialog(pantallaLogin, "Error cargando las tablas.", "Error", JOptionPane.ERROR_MESSAGE);
			try {
				tm.eliminarTabla("Clientes");
				tm.eliminarTabla("Cuentas");
				tm.eliminarTabla("Movimientos");
				tm.eliminarTabla("Tarjetas");
			} catch (DefinicionDatosException e) {
				e.printStackTrace();
			}
		} catch (ServicioException e2) {
			e2.printStackTrace();
			tm.eliminarTabla("Clientes");
			tm.eliminarTabla("Cuentas");
			tm.eliminarTabla("Movimientos");
			tm.eliminarTabla("Tarjetas");
		}
	}
}