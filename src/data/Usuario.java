package data;

public class Usuario {

	private String nombreUsuario;
	private String nombreFull;
	private String email;
	private String password;
	
	public Usuario(String nombreUsuario, String nombreFull, String email, String password) {
        this.setNombreUsuario(nombreUsuario);
        this.setNombreFull(nombreFull);
        this.setEmail(email);
        this.setPassword(password);
    }

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombreFull() {
		return nombreFull;
	}

	public void setNombreFull(String nombreFull) {
		this.nombreFull = nombreFull;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
