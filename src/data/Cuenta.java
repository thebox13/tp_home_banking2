package data;

public class Cuenta {
	private String usuario;
	private String tipo_cuenta;
	private int nro_cuenta;
	private float dinero;
	
	public Cuenta(String usuario, String tipo_cuenta, int nro_cuenta, float dinero) {
		this.setUsuario(usuario);
		this.setTipo_cuenta(tipo_cuenta);
		this.setNro_cuenta(nro_cuenta);
		this.setDinero(dinero);
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getTipo_cuenta() {
		return tipo_cuenta;
	}
	public void setTipo_cuenta(String tipo_cuenta) {
		this.tipo_cuenta = tipo_cuenta;
	}
	public int getNro_cuenta() {
		return nro_cuenta;
	}
	public void setNro_cuenta(int nro_cuenta) {
		this.nro_cuenta = nro_cuenta;
	}
	public float getDinero() {
		return dinero;
	}
	public void setDinero(float dinero) {
		this.dinero = dinero;
	}
	
	

}
