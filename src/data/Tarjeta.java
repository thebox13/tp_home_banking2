package data;

public class Tarjeta {
	private String usuario;
	private int nroTarjeta;
	private float consumo;
	private float limite;
	
	public Tarjeta(String usuario, int nroTarjeta, float consumo, float limite) {
		this.setUsuario(usuario);
		this.setNroTarjeta(nroTarjeta);
		this.setConsumo(consumo);
		this.setLimite(limite);
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public float getConsumo() {
		return consumo;
	}
	public void setConsumo(float consumo) {
		this.consumo = consumo;
	}
	public float getLimite() {
		return limite;
	}
	public void setLimite(float limite) {
		this.limite = limite;
	}

	public int getNroTarjeta() {
		return nroTarjeta;
	}

	public void setNroTarjeta(int nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
}
