package data;

public class Movimiento {
	private int nro_cuenta;
	private float movimiento_dinero;
	
	public Movimiento(int nro_cuenta, float movimiento_dinero) {
		this.setNro_cuenta(nro_cuenta);
		this.setMovimientoDinero(movimiento_dinero);
	}
	
	public int getNro_cuenta() {
		return nro_cuenta;
	}
	public void setNro_cuenta(int nro_cuenta) {
		this.nro_cuenta = nro_cuenta;
	}
	public float getMovimientoDinero() {
		return movimiento_dinero;
	}
	public void setMovimientoDinero(float movimiento_dinero) {
		this.movimiento_dinero = movimiento_dinero;
	}
}
