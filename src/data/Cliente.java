package data;

public class Cliente extends Usuario {
	private String dni;
	private String numeroContacto;
	
	public Cliente (String nombreUsuario, String nombreFull, String email, String password, String dni, String numeroContacto) {
		super(nombreUsuario, nombreFull, email, password);
		this.setDni(dni);
		this.setNumeroContacto(numeroContacto);
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNumeroContacto() {
		return numeroContacto;
	}

	public void setNumeroContacto(String numeroContacto) {
		this.numeroContacto = numeroContacto;
	}
}