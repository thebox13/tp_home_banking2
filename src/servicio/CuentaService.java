package servicio;

import java.util.List;

import daos.ClienteDAO;
import daos.CuentaDAO;
import daos.CuentaDAOH2Impl;
import data.Cliente;
import data.Cuenta;
import exceptions.*;
import utils.validations.ValidarTexto;
import utils.validations.ValidarNumeros;


public class CuentaService {
	private CuentaDAO cuentaDao;
	
	public CuentaService()  {
		this.cuentaDao = new CuentaDAOH2Impl();
	}
	
	public void create(Cuenta cuenta) throws ServicioException {
		validateData(cuenta);
		
		try {
			cuentaDao.crear(cuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public void delete(String nombreCliente) throws ServicioException {
		try {
			cuentaDao.borrar(nombreCliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public void delete(int nroCuenta) throws ServicioException {
		try {
			cuentaDao.borrar(nroCuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public List<Cuenta> findByClientName(String nombreCliente) throws ServicioException {
		try {
			return cuentaDao.obtenerPorNombre(nombreCliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public List<Cuenta> listAll() throws ServicioException {
		try {
			return cuentaDao.listarTodo();
			} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public int ultimaCuenta() throws ServicioException {
		try {
			return cuentaDao.ultimaCuenta();
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	private void validateData(Cuenta cuenta) throws ServicioException {
		ValidarTexto.validateNotEmpty(cuenta.getTipo_cuenta());
		ValidarTexto.validateNotEmpty(cuenta.getUsuario());
		ValidarNumeros.validateRange(cuenta.getNro_cuenta(), 0, 999999999);
		ValidarNumeros.validateRange(cuenta.getDinero(), 0, 999999999);
	}

	public void update(Cuenta cuenta) throws ServicioException {
		try {
			cuentaDao.actualizar(cuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public boolean validarCuenta(String nombreCliente, int nroCuenta) throws ServicioException {
		try {
			return cuentaDao.verificarCuenta(nombreCliente, nroCuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public Cuenta obtenerPorCuenta(int nroCuenta) throws ServicioException {
		try {
			return cuentaDao.obtenerPorCuenta(nroCuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
}
