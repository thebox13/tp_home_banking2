package servicio;

import java.util.List;
import data.Cliente;
import daos.ClienteDAO;
import daos.ClienteDAOH2Impl;
import exceptions.*;
import utils.validations.ValidarTexto;

public class ClienteService {
	
	private ClienteDAO clienteDao;
	
	public ClienteService() {
		this.clienteDao = new ClienteDAOH2Impl();
	}

	public void create(Cliente cliente) throws ServicioException {
		validateData(cliente);
		
		try {
			clienteDao.crear(cliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public void delete(String nombreCliente) throws ServicioException {
		try {
			clienteDao.borrar(nombreCliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public void update(Cliente cliente) throws ServicioException {
		validateData(cliente);
		
		try {
			clienteDao.actualizar(cliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public Cliente findByName(String nombreCliente) throws ServicioException {
		try {
			return clienteDao.obtenerPorNombre(nombreCliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public List<Cliente> listAll() throws ServicioException {
		try {
			return clienteDao.obtenerTodo();
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public boolean verificarPass(String nombreCliente, String password) throws ServicioException {
		Cliente cliente;
		try {
			cliente = clienteDao.obtenerPorNombre(nombreCliente);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
		
		if (cliente == null) {
			return false;
		}
		return cliente.getPassword().equals(password);
	}
	
	private void validateData(Cliente cliente) throws ServicioException {
		ValidarTexto.validateNotEmpty(cliente.getEmail());
		ValidarTexto.validateNotEmpty(cliente.getNombreFull());
		ValidarTexto.validateNotEmpty(cliente.getDni());
		ValidarTexto.validateNotEmpty(cliente.getPassword());
		ValidarTexto.validateNotEmpty(cliente.getNombreUsuario());
		
		ValidarTexto.validateEmailFormat(cliente.getEmail());
		
		ValidarTexto.validateNumbersOnly(cliente.getDni());
		
		ValidarTexto.validateOnlyAlphabetsAndSpace(cliente.getNombreUsuario());
	}
	
}

