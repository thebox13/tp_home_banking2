package servicio;

import java.util.List;

import data.Cliente;
import data.Tarjeta;
import daos.ClienteDAO;
import daos.ClienteDAOH2Impl;
import daos.TarjetaDAO;
import daos.TarjetaDAOH2Impl;
import exceptions.*;
import utils.validations.ValidarNumeros;
import utils.validations.ValidarTexto;

public class TarjetaService {
	private TarjetaDAO tarjetaDao;
	
	public TarjetaService() {
		this.tarjetaDao = new TarjetaDAOH2Impl();
	}

	public void create(Tarjeta tarjeta) throws ServicioException {
		validateData(tarjeta);
		
		try {
			tarjetaDao.crear(tarjeta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public void delete(int nroTarjeta) throws ServicioException {
		try {
			tarjetaDao.borrar(nroTarjeta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}

	public void update(Tarjeta tarjeta) throws ServicioException {
		validateData(tarjeta);
		
		try {
			tarjetaDao.actualizar(tarjeta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public List<Tarjeta> findByClientName(String nombreUsuario) throws ServicioException {
		try {
			return tarjetaDao.obtenerPorNombre(nombreUsuario);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	private void validateData(Tarjeta tarjeta) throws ServicioException {
		ValidarTexto.validateNotEmpty(tarjeta.getUsuario());
		ValidarNumeros.validateRange(tarjeta.getNroTarjeta(), 0, 999999999);
		ValidarNumeros.validateRange(tarjeta.getConsumo(), 0, 999999999);
		ValidarNumeros.validateRange(tarjeta.getLimite(), 0, 999999999);
	}

}