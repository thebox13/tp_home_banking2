package servicio;

import java.util.List;
import data.Movimiento;
import daos.MovimientoDAO;
import daos.MovimientoDAOH2Impl;
import exceptions.*;
import utils.validations.ValidarNumeros;
import utils.validations.ValidarTexto;

public class MovimientoService {
	private MovimientoDAO movimientoDao;
	
	public MovimientoService() {
		this.movimientoDao = new MovimientoDAOH2Impl();
	}
	
	public void create(Movimiento movimiento) throws ServicioException {
		validateData(movimiento);
		
		try {
			movimientoDao.crear(movimiento);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public void delete(int nroCuenta) throws ServicioException {
		try {
			movimientoDao.borrar(nroCuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	public List<Movimiento> findByAccountNr(int nroCuenta) throws ServicioException {
		try {
			return movimientoDao.obtenerPorNroCuenta(nroCuenta);
		} catch (AccesoDatosException e) {
			e.printStackTrace();
			throw new ServicioException(e.getMessage(), e.getCause());
		}
	}
	
	private void validateData(Movimiento movimiento) throws ServicioException {
		ValidarNumeros.validateRange(movimiento.getNro_cuenta(), 0, 999999999);
		ValidarNumeros.validateRange(movimiento.getMovimientoDinero(), -999999999, 999999999);
	}
}
