package controlador;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import data.Cliente;
import data.Cuenta;
import db.TableManager;
import exceptions.DefinicionDatosException;
import exceptions.ServicioException;
import vista.ClienteCrearPantallaPanel;
import vista.ClienteManejarPantallaPanel;
import vista.CuentaActualizarPantallaPanel;
import vista.CuentaCrearPantallaPanel;
import vista.CuentaManejarPantallaPanel;
import vista.ClienteActualizarPantallaPanel;
import vista.AdminHomePanel;

public class ControladorAdminPanelManager {
	private JFrame frame;
	private AdminHomePanel homePanel;
	private ClienteCrearPantallaPanel clienteCrearPantallaPanel;
	private ClienteManejarPantallaPanel clienteManejarPantallaPanel;
	private ClienteActualizarPantallaPanel clienteActualizarPantallaPanel;
	private CuentaCrearPantallaPanel cuentaCrearPantallaPanel;
	private CuentaManejarPantallaPanel cuentaManejarPantallaPanel;
	private CuentaActualizarPantallaPanel cuentaActualizarPantallaPanel;
	
	public ControladorAdminPanelManager() {
	
	}
	
	public void buildManager() throws ServicioException {
		frame= new JFrame("Home Banking - Administracion");
		frame.setBounds(100, 100, 800, 800);
		frame.setSize(800, 800);
		homePanel = new AdminHomePanel(this);
		clienteCrearPantallaPanel = new ClienteCrearPantallaPanel(this);
		clienteManejarPantallaPanel = new ClienteManejarPantallaPanel(this);
		cuentaCrearPantallaPanel = new CuentaCrearPantallaPanel(this);
		cuentaManejarPantallaPanel = new CuentaManejarPantallaPanel(this);
	}
		
	public void mostrarFrame() {
		frame.setVisible(true);
	}
		
	public void mostrarExit() {
		int response = JOptionPane.showConfirmDialog(frame, "Seguro?");
		
		if(response == JOptionPane.OK_OPTION) {
			TableManager tm = new TableManager();
			try {
				tm.eliminarTabla("Clientes");
			} catch (DefinicionDatosException e) {
				e.printStackTrace();
			}
			
			System.exit(0);
		 }
	}
	
	public void mostrarHomePanel() {
		frame.getContentPane().removeAll();
		frame.getContentPane().add(homePanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}

	public void mostrarClienteCrearPantallaPanel() {
		clienteCrearPantallaPanel = new ClienteCrearPantallaPanel(this); 
		frame.getContentPane().removeAll();
		frame.getContentPane().add(clienteCrearPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void mostrarCuentaCrearPantallaPanel() {
		cuentaCrearPantallaPanel = new CuentaCrearPantallaPanel(this);
		cuentaCrearPantallaPanel.getFieldsPanel().getNroCuentaTxt().setText("-------------");
		cuentaCrearPantallaPanel.getFieldsPanel().getNroCuentaTxt().setEditable(false);
		frame.getContentPane().removeAll();
		frame.getContentPane().add(cuentaCrearPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void mostrarClienteActualizarPantallaPanel(Cliente toUpdate) {
		clienteActualizarPantallaPanel = new ClienteActualizarPantallaPanel(this);
		clienteActualizarPantallaPanel.getFieldsPanel().getUsuarioNombreTxt().setEditable(false);
		clienteActualizarPantallaPanel.getFieldsPanel().getUsuarioNombreTxt().setText(toUpdate.getNombreUsuario());
		clienteActualizarPantallaPanel.getFieldsPanel().getNombreFullTxt().setText(toUpdate.getNombreFull());
		clienteActualizarPantallaPanel.getFieldsPanel().getEmailTxt().setText(toUpdate.getEmail());
		clienteActualizarPantallaPanel.getFieldsPanel().getPasswordTxt().setText(toUpdate.getPassword());
		clienteActualizarPantallaPanel.getFieldsPanel().getDniTxt().setText(toUpdate.getDni());
		clienteActualizarPantallaPanel.getFieldsPanel().getNumeroContactoTxt().setText(toUpdate.getNumeroContacto());
		
		frame.getContentPane().removeAll();
		frame.getContentPane().add(clienteActualizarPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void mostrarCuentaActualizarPantallaPanel(Cuenta toUpdate) {
		cuentaActualizarPantallaPanel = new CuentaActualizarPantallaPanel(this);
		cuentaActualizarPantallaPanel.getFieldsPanel().getUsuarioTxt().setEditable(false);
		cuentaActualizarPantallaPanel.getFieldsPanel().getUsuarioTxt().setText(toUpdate.getUsuario());
		cuentaActualizarPantallaPanel.getFieldsPanel().getNroCuentaTxt().setEditable(false);
		cuentaActualizarPantallaPanel.getFieldsPanel().getNroCuentaTxt().setText(String.valueOf(toUpdate.getNro_cuenta()));
		cuentaActualizarPantallaPanel.getFieldsPanel().getTipoCuentaTxt().setText(toUpdate.getTipo_cuenta());
		cuentaActualizarPantallaPanel.getFieldsPanel().getDineroTxt().setText(String.valueOf(toUpdate.getDinero()));
		
		frame.getContentPane().removeAll();
		frame.getContentPane().add(cuentaActualizarPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void mostrarClienteManejarPantallaPanel() throws ServicioException {
		clienteManejarPantallaPanel = new ClienteManejarPantallaPanel(this); 
		frame.getContentPane().removeAll();
		frame.getContentPane().add(clienteManejarPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void mostrarCuentaManejarPantallaPanel() throws ServicioException {
		cuentaManejarPantallaPanel = new CuentaManejarPantallaPanel(this); 
		frame.getContentPane().removeAll();
		frame.getContentPane().add(cuentaManejarPantallaPanel);
		frame.getContentPane().validate();
		frame.getContentPane().repaint();
	}
	
	public void setVisible(boolean b) {
		frame.setVisible(b);
	}
}
