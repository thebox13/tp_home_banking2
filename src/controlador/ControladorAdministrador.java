package controlador;

import vista.PantallaAdministrador;

public class ControladorAdministrador {
	static PantallaAdministrador adminCliente = new PantallaAdministrador();
	
	public static void mostrar() {
		adminCliente.setVisible(true);
	}
	
	public static void ocultar() {
		adminCliente.setVisible(false);
	}
	
	public static void exitPrograma() {
		System.exit(0);
	}
}
