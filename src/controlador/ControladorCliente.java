package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import data.Cliente;
import data.Cuenta;
import data.Tarjeta;
import exceptions.ServicioException;
import servicio.ClienteService;
import servicio.CuentaService;
import servicio.TarjetaService;
import vista.VentanaUsuario;

public class ControladorCliente {

    private VentanaUsuario ventanaUsuario;
    private Cliente usuario;

    public void mostrarInformacionUsuario(String cliente) throws ServicioException {
    	CuentaService cuentaDao = new CuentaService();
    	TarjetaService tarjetaDao = new TarjetaService();
    	List<Cuenta> cuentas = cuentaDao.findByClientName(cliente);
    	List<Tarjeta> tarjetas = tarjetaDao.findByClientName(cliente);
    	
        ventanaUsuario = new VentanaUsuario(cliente, cuentas, tarjetas);
    }

}