package controlador;

import exceptions.ServicioException;
import servicio.ClienteService;
import vista.PantallaLogin;

public class ControladorLogin {
	static PantallaLogin login = new PantallaLogin();
	
	public static void mostrar() {
		login.setVisible(true);
	}
	
	public static void ocultar() {
		login.setVisible(false);
	}
	
	public static void verificarLogin(String usuario, String password) throws ServicioException {
		ClienteService userDao = new ClienteService();
		if (userDao.verificarPass(usuario, password)) {
			ocultar();
			if (usuario.equals("administrador")) {
				ControladorAdministrador cAdmin = new ControladorAdministrador();
				cAdmin.mostrar();
			} else {
				ControladorUsuario cUsuario = new ControladorUsuario(usuario);
				cUsuario.mostrar();
			}
		} else {
			login.usuarioPassIncorrecto();
		}
	}
	
	public static void exitPrograma() {
		System.exit(0);
	}
}
