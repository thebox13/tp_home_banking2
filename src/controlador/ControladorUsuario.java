package controlador;

import vista.PantallaAdministrador;
import vista.PantallaUsuario;

public class ControladorUsuario {
	static PantallaUsuario adminUsuario;
	
	public ControladorUsuario(String usuario) {
		adminUsuario = new PantallaUsuario(usuario);
	}
	
	public static void mostrar() {
		adminUsuario.setVisible(true);
	}
	
	public static void ocultar() {
		adminUsuario.setVisible(false);
	}
	
	public static void exitPrograma() {
		System.exit(0);
	}
}
