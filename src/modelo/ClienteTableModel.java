package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import data.Cliente;

public class ClienteTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static final int COLUMN_USERNAME = 0;
	private static final int COLUMN_FULLNAME = 1;
	private static final int COLUMN_EMAIL = 2;
	private static final int COLUMN_PASSWORD = 3;
	private static final int COLUMN_GOVID = 4;
	private static final int COLUMN_CONTACTNUM = 5;

	private String[] columnNames = { "Login", "Nombre", "Email", "Contrasena", "DNI", "Tel. de Contacto" };

	private Class[] columnTypes = { String.class, String.class, String.class, String.class, String.class, String.class };

	private List<Cliente> content;

	public ClienteTableModel() {
		content = new ArrayList<Cliente>();
	}

	public ClienteTableModel(List<Cliente> initialContent) {
		content = initialContent;
	}

	@Override
	public int getRowCount() {
		return content.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		 Cliente c = content.get(rowIndex);
		 Object result = null; 
		 
		 switch (columnIndex) { 
		 	case COLUMN_USERNAME: 
		 		result = c.getNombreUsuario(); 
		 		break; 
	 		case COLUMN_FULLNAME: 
	 			result = c.getNombreFull(); 
	 			break;
	 		case COLUMN_EMAIL: 
	 			result = c.getEmail(); 
	 			break; 
 			case COLUMN_PASSWORD:
 				result = c.getPassword();
 				break;
 			case COLUMN_GOVID: 
 				result = c.getDni(); 
 				break;
 			case COLUMN_CONTACTNUM:
 				result = c.getNumeroContacto();
 				break;
			default: 
				result = new String(""); 
		}
		 
		return result;
	}
	
	public String getColumnName(int col) { 
		return columnNames[col]; 
	}
	
	public Class getColumnClass(int col) { 
		return columnTypes[col]; 
	}
	
	public List<Cliente> getContent() { 
		return content; 
	}
	
	public void setContent(List<Cliente> content) { 
		this.content = content; 
	}

}
