package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import data.Cliente;
import data.Cuenta;

public class CuentaTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private static final int COLUMN_USERNAME = 0;
	private static final int COLUMN_TIPOCUENTA = 1;
	private static final int COLUMN_NUMEROCUENTA = 2;
	private static final int COLUMN_DINERO = 3;

	private String[] columnNames = { "Cliente", "Tipo Cuenta", "Numero Cuenta", "Dinero" };

	private Class[] columnTypes = { String.class, String.class, int.class, float.class };

	private List<Cuenta> content;

	public CuentaTableModel() {
		content = new ArrayList<Cuenta>();
	}

	public CuentaTableModel(List<Cuenta> initialContent) {
		content = initialContent;
	}

	@Override
	public int getRowCount() {
		return content.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		 Cuenta c = content.get(rowIndex);
		 Object result = null; 
		 
		 switch (columnIndex) { 
		 	case COLUMN_USERNAME: 
		 		result = c.getUsuario();
		 		break; 
	 		case COLUMN_TIPOCUENTA: 
	 			result = c.getTipo_cuenta(); 
	 			break;
	 		case COLUMN_NUMEROCUENTA: 
	 			result = c.getNro_cuenta(); 
	 			break; 
 			case COLUMN_DINERO:
 				result = c.getDinero();
 				break;
			default: 
				result = new String(""); 
		}
		 
		return result;
	}
	
	public String getColumnName(int col) { 
		return columnNames[col]; 
	}
	
	public Class getColumnClass(int col) { 
		return columnTypes[col]; 
	}
	
	public List<Cuenta> getContent() { 
		return content; 
	}
	
	public void setContent(List<Cuenta> content) { 
		this.content = content; 
	}

}
