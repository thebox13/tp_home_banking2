package utils.validations;

import exceptions.*;

public class ValidarNumeros {
	
	public static void validateRange(int number, int from, int to) throws NumeroInvalidoException {
		if(number < from || number > to) {
			throw new RangoDeNumeroException();
		}
	}
	
	public static void validateRange(long number, long from, long to) throws NumeroInvalidoException {
		if(number < from || number > to) {
			throw new RangoDeNumeroException();
		}
	}
	
	public static void validateRange(float number, float from, float to) throws NumeroInvalidoException {
		if(number < from || number > to) {
			throw new RangoDeNumeroException();
		}
	}
	
	public static void validateRange(double number, double from, double to) throws NumeroInvalidoException {
		if(number < from || number > to) {
			throw new RangoDeNumeroException();
		}
	}
	
	public static void validateRange(int number, boolean leftRange, int limit) throws NumeroInvalidoException {
		if(leftRange) {
			if(number > limit)
				throw new RangoDeNumeroException("Number is greater than " + limit);
		}
		else {
			if(number < limit)
				throw new RangoDeNumeroException("Number is less than " + limit);
		}
	}
	
	public static void validateRange(long number, boolean leftRange, long limit) throws NumeroInvalidoException {
		if(leftRange) {
			if(number > limit)
				throw new RangoDeNumeroException("Number is greater than " + limit);
		}
		else {
			if(number < limit)
				throw new RangoDeNumeroException("Number is less than " + limit);
		}
	}
	
	public static void validateRange(float number, boolean leftRange, float limit) throws NumeroInvalidoException {
		if(leftRange) {
			if(number > limit)
				throw new RangoDeNumeroException("Number is greater than " + limit);
		}
		else {
			if(number < limit)
				throw new RangoDeNumeroException("Number is less than " + limit);
		}
	}
	
	public static void validateRange(double number, boolean leftRange, double limit) throws NumeroInvalidoException {
		if(leftRange) {
			if(number > limit)
				throw new RangoDeNumeroException("Number is greater than " + limit);
		}
		else {
			if(number < limit)
				throw new RangoDeNumeroException("Number is less than " + limit);
		}
	}
}
