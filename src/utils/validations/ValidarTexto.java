package utils.validations;

import exceptions.*;

public class ValidarTexto {

	public static void validateNotEmpty(String text) throws TextoInvalidoException {
		if(text.isBlank()) {
			throw new TextoVacioException("Empty string.");
		}
	}
	
	public static void validateLength(String text, int numberOfChars) throws TextoInvalidoException {
		if(text.length() != numberOfChars) {
			throw new LargoInvalidoException("Invalid length.");
		}
	}
	
	public static void validateNumbersOnly(String text) throws TextoInvalidoException {
		if(!text.matches("[0-9]+")) {
			throw new NoEsNumeroException("String is not numeric.");
		}
	}
	
	public static void validateOnlyAlphabetsAndSpace(String text) throws TextoInvalidoException {
		if(!text.matches("^[a-zA-Z\\s]*$")) {
			throw new NoEsLetraException("String contains invalid digits.");
		}
	}
	
	public static void validateEmailFormat(String text) throws TextoInvalidoException {
		if(!text.matches("^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$")) {
			throw new FormatoEmailException("Invalid email format.");
		}
	}
	
	public static void validateFloatsOnly(String text) throws TextoInvalidoException {
		if(!text.matches("[-+]?[0-9]*([.][0-9]+)?")) {
			throw new NoEsNumeroException("String is not numeric.");
		}
	}
}
