package vista;

import java.awt.GridLayout;

import javax.swing.*;

import controlador.ControladorAdminPanelManager;

public class ClienteCampoPanel extends CampoPanel {
	private static final long serialVersionUID = 1L;
	private JTextField usuarioNombreTxt;
	private JTextField nombreFullTxt;
	private JTextField emailTxt;
	private JTextField passwordTxt;
	private JTextField dniTxt;
	private JTextField numeroContactoTxt;
	

	public ClienteCampoPanel(ControladorAdminPanelManager panelManager) {
		super(panelManager);
	}
	
	@Override
	public void buildForm() {
		this.setLayout(new GridLayout(12,2));
		this.setSize(800,800);
		JLabel nombreUsuarioLbl = new JLabel("Login:");
		JLabel passLbl = new JLabel("Contrasena:");
		JLabel nombreFullLbl = new JLabel("Nombre y apellido");
		JLabel emailLbl = new JLabel("Email:");
		JLabel numeroContactoLbl = new JLabel("Telefono:");
		JLabel dniLbl = new JLabel("DNI:");
		
		usuarioNombreTxt = new JTextField("");
		passwordTxt = new JTextField("");
		nombreFullTxt = new JTextField("");
		emailTxt = new JTextField("");
		numeroContactoTxt = new JTextField("");
		dniTxt = new JTextField("");
		
		this.add(nombreUsuarioLbl);
		this.add(usuarioNombreTxt);
		this.add(nombreFullLbl);
		this.add(nombreFullTxt);
		this.add(emailLbl);
		this.add(emailTxt);
		this.add(passLbl);
		this.add(passwordTxt);
		this.add(dniLbl);
		this.add(dniTxt);
		this.add(numeroContactoLbl);
		this.add(numeroContactoTxt);
	}
	
    public JTextField getUsuarioNombreTxt() {
        return usuarioNombreTxt;
    }

    public void setUsuarioNombreTxt(JTextField usuarioNombreTxt) {
        this.usuarioNombreTxt = usuarioNombreTxt;
    }
    
    public JTextField getNombreFullTxt() {
        return nombreFullTxt;
    }

    public void setNombreFullTxt(JTextField nombreFullTxt) {
        this.nombreFullTxt = nombreFullTxt;
    }

    public JTextField getEmailTxt() {
        return emailTxt;
    }

    public void setEmailTxt(JTextField emailTxt) {
        this.emailTxt = emailTxt;
    }

    public JTextField getPasswordTxt() {
        return passwordTxt;
    }

    public void setPasswordTxt(JTextField passwordTxt) {
        this.passwordTxt = passwordTxt;
    }

    public JTextField getDniTxt() {
        return dniTxt;
    }

    public void setDniTxt(JTextField dniTxt) {
        this.dniTxt = dniTxt;
    }
    public JTextField getNumeroContactoTxt() {
        return numeroContactoTxt;
    }

    public void setNumeroContactoTxt(JTextField numeroContactoTxt) {
        this.numeroContactoTxt = numeroContactoTxt;
    }
}
