package vista;

import controlador.ControladorCliente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import controlador.ControladorAdminPanelManager;
import controlador.ControladorUsuarioPanelManager;
import exceptions.ServicioException;

public class PantallaUsuario implements ActionListener {
	ControladorUsuarioPanelManager panelManager = null;
	
	public PantallaUsuario(String usuario) {
		try {
			ControladorCliente cCliente = new ControladorCliente();
			cCliente.mostrarInformacionUsuario(usuario);
		} catch (ServicioException e2) {
			e2.printStackTrace();
		}
		
	}
	
	public void usuarioPassIncorrecto() {
		JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos: ", "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

	public void setVisible(boolean b) {
		panelManager.setVisible(b);
	}
}
