package vista;

import javax.swing.*;

import data.Cuenta;
import exceptions.ServicioException;
import servicio.CuentaService;

import java.awt.*;
import java.awt.event.*;

public class VentanaTransaccion extends JFrame implements ActionListener {
    
    private JLabel clienteLabel, desdeLabel, haciaLabel, montoLabel;
    private JTextField desdeField, haciaField, montoField;
    private JButton enviarButton, volverButton;
    String cliente;
    
    public VentanaTransaccion(String cliente) {
    	this.cliente = cliente;
        setTitle("Transacción");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(5, 2));
        
        clienteLabel = new JLabel("Cliente:");
        add(clienteLabel);
        JLabel nombreClienteLabel = new JLabel(cliente);
        add(nombreClienteLabel);
        
        desdeLabel = new JLabel("Desde:");
        add(desdeLabel);
        desdeField = new JTextField(20);
        add(desdeField);
        
        haciaLabel = new JLabel("Hacia:");
        add(haciaLabel);
        haciaField = new JTextField(20);
        add(haciaField);
        
        montoLabel = new JLabel("Monto:");
        add(montoLabel);
        montoField = new JTextField(20);
        add(montoField);
        
        enviarButton = new JButton("Enviar");
        enviarButton.addActionListener(this);
        add(enviarButton);
        
        volverButton = new JButton("Volver");
        volverButton.addActionListener(this);
        add(volverButton);
        
        mostrar();
    }
    
    public void mostrar() {
    	setVisible(true);
    }
    
    public void ocultar() {
    	setVisible(false);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == enviarButton) {
            int desde = Integer.parseInt(desdeField.getText());
            int hacia = Integer.parseInt(haciaField.getText());
            Float monto = Float.parseFloat(montoField.getText());
            
            CuentaService cuentaDao = new CuentaService();
            try {
            	boolean error = false;
				if (! cuentaDao.validarCuenta(this.cliente, desde)) {
					error = true;
					JOptionPane.showMessageDialog(this, "Usted no es dueño de esa cuenta", "Error", JOptionPane.ERROR_MESSAGE);
					dispose();
				} else if (cuentaDao.obtenerPorCuenta(hacia) == null) {
					error = true;
					JOptionPane.showMessageDialog(this, "La cuenta no existe", "Error", JOptionPane.ERROR_MESSAGE);
					dispose();
				}else if (desde == hacia) {
					error = true;
					JOptionPane.showMessageDialog(this, "Las cuentas son iguales", "Error", JOptionPane.ERROR_MESSAGE);
					dispose();
				} else if (monto <= 0) {
					error = true;
					JOptionPane.showMessageDialog(this, "No puede hacer transacciones negativas", "Error", JOptionPane.ERROR_MESSAGE);
					dispose();
				}
				Cuenta desdeCuenta = cuentaDao.obtenerPorCuenta(desde);
				float dineroCuentaDesde = desdeCuenta.getDinero();
				if (monto > dineroCuentaDesde && !error) {
					error = true;
					JOptionPane.showMessageDialog(this, "Usted no posee ese dinero", "Error", JOptionPane.ERROR_MESSAGE);
					dispose();
				}
				if (!error) {
					Cuenta haciaCuenta = cuentaDao.obtenerPorCuenta(hacia);
					float dineroCuentaHacia = desdeCuenta.getDinero();
					desdeCuenta.setDinero(dineroCuentaDesde - monto);
					haciaCuenta.setDinero(dineroCuentaHacia + monto);
					JOptionPane.showMessageDialog(this, "Dinero enviado", "Info", JOptionPane.INFORMATION_MESSAGE);
					dispose();
				}
			} catch (ServicioException e1) {
				e1.printStackTrace();
			}
        }
        else if (e.getSource() == volverButton) {
            dispose();
        }
    }
}