package vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import exceptions.ServicioException;
import controlador.ControladorAdminPanelManager;
import controlador.ControladorUsuarioPanelManager;

public class UsuarioHomePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	protected ControladorUsuarioPanelManager panelManager;
	private JButton enviarBttn;
	private JButton estadoCuentaBttn;
	private JButton administrarTarjetaBttn;
	private JButton salirBttn;
	
	public UsuarioHomePanel(ControladorUsuarioPanelManager controladorUsuarioPanelManager) {
		this.panelManager = controladorUsuarioPanelManager;
		buildHomePanel();
	}

	public void buildHomePanel() {
		this.setLayout(new FlowLayout());
		this.enviarBttn = new JButton("Enviar Dinero");
		this.estadoCuentaBttn = new JButton("Estado Cuenta");
		this.administrarTarjetaBttn = new JButton("Administrar Tarjeta");
		this.salirBttn = new JButton("Salir");
		
		this.add(enviarBttn);
		this.add(estadoCuentaBttn);
		this.add(administrarTarjetaBttn);
		this.add(salirBttn);
		
		/*
		this.enviarBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					panelManager.mostrarClienteManejarPantallaPanel();
				} catch (ServicioException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		 });
		
		this.estadoCuentaBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					panelManager.mostrarCuentaManejarPantallaPanel();
				} catch (ServicioException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		 });
		 */
		
		this.salirBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.mostrarExit();
			}
		});
	}
}
