package vista;

import javax.swing.JOptionPane;

import data.Cliente;
import exceptions.ServicioException;
import servicio.ClienteService;
import controlador.ControladorAdminPanelManager;

public class ClienteActualizarPantallaPanel extends EditarPantallaPanel {
	private static final long serialVersionUID = 1L;

	public ClienteActualizarPantallaPanel(ControladorAdminPanelManager panelManager) {
		super(panelManager);
	}

	@Override
	public void setFieldsPanel() {
		this.fieldsPanel = new ClienteCampoPanel(panelManager);

	}

	@Override
	public void executeOnSave() {
		ClienteCampoPanel cfp = (ClienteCampoPanel)this.fieldsPanel;
		
		Cliente customer = new Cliente(cfp.getUsuarioNombreTxt().getText(), cfp.getNombreFullTxt().getText(), cfp.getEmailTxt().getText(), cfp.getPasswordTxt().getText(), cfp.getDniTxt().getText(), cfp.getNumeroContactoTxt().getText());
		ClienteService cs = new ClienteService();
		
		try {
			cs.update(customer);
			panelManager.mostrarClienteManejarPantallaPanel();
		} catch (ServicioException e) {
			JOptionPane.showMessageDialog(fieldsPanel, "No se pudo crear el usuario: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	@Override
	public void executeOnCancel() throws ServicioException {
		panelManager.mostrarClienteManejarPantallaPanel();

	}
	
	public ClienteCampoPanel getFieldsPanel() {
		return (ClienteCampoPanel)this.fieldsPanel;
	}
}
