package vista;

import javax.swing.*;

import data.Cuenta;
import data.Tarjeta;
import exceptions.ServicioException;
import modelo.ClienteTableModel;
import modelo.CuentaTableModel;
import servicio.CuentaService;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class VentanaUsuario extends JFrame {

    private JLabel nombreLabel;
    private JLabel cuentasLabel;
    private JLabel tarjetasLabel;
    private JList<String> cuentasList;
    private JList<String> tarjetasList;
    private JButton exitButton;
    private JButton enviarDineroButton;

	private JTable cuentaTable;
	private CuentaTableModel modelCuenta;

	private JScrollPane scrollPaneForTableCuenta;

    public VentanaUsuario(String nombre, List<Cuenta> cuentas, List<Tarjeta> tarjetas) {
        setTitle("Información de usuario");
        setSize(800, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        nombreLabel = new JLabel("Nombre de usuario: " + nombre);
        cuentasLabel = new JLabel("Cuentas:");
        modelCuenta = new CuentaTableModel();
		cuentaTable = new JTable(modelCuenta);
		scrollPaneForTableCuenta = new JScrollPane(cuentaTable);
		modelCuenta.setContent(cuentas);
		modelCuenta.fireTableDataChanged();
		
        tarjetasLabel = new JLabel("Tarjetas:");
        
        exitButton = new JButton("Salir");
        enviarDineroButton = new JButton("Enviar Dinero");

        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 5, 5, 5);

        c.gridx = 0;
        c.gridy = 0;
        add(nombreLabel, c);

        c.gridx = 0;
        c.gridy = 1;
        add(cuentasLabel, c);

        c.gridx = 0;
        c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
		add(scrollPaneForTableCuenta, c);

        c.gridx = 0;
        c.gridy = 3;
        add(tarjetasLabel, c);

        c.gridx = 0;
        c.gridy = 4;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(new JScrollPane(tarjetasList), c);

        c.gridx = 0;
        c.gridy = 5;
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0;
        c.weighty = 0;
        add(exitButton, c);
        
        c.gridx = 1;
        c.gridy = 5;
        c.fill = GridBagConstraints.NONE;
        c.weightx = 0;
        c.weighty = 0;
        add(enviarDineroButton, c);

        setVisible(true);
        
        this.exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
        
        this.enviarDineroButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				VentanaTransaccion vTransaccion = new VentanaTransaccion(nombre);
			}
		});
        
        this.addWindowFocusListener(new WindowAdapter() {
            public void windowGainedFocus(WindowEvent e) {
            	CuentaService cuentaDao = new CuentaService();
            	try {
					List<Cuenta> newCuentas = cuentaDao.findByClientName(nombre);
	        		modelCuenta.setContent(newCuentas);
					modelCuenta.fireTableDataChanged();
				} catch (ServicioException e1) {
					e1.printStackTrace();
				}
            }
        });
    }

}