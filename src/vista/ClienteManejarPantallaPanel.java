package vista;

import javax.swing.JOptionPane;

import data.Cliente;
import exceptions.ServicioException;
import servicio.ClienteService;
import controlador.ControladorAdminPanelManager;

public class ClienteManejarPantallaPanel extends ManejoPantallaPanel {
	private static final long serialVersionUID = 1L;

	public ClienteManejarPantallaPanel(ControladorAdminPanelManager panelManager) throws ServicioException {
		super(panelManager);
		this.buildScreenPanel();
	}

	@Override
	public void setTablePanel() throws ServicioException {
		this.tablePanel = new ClienteTablaPanel(panelManager);
	}

	@Override
	public void executeOnNewBttnClicked() {
		panelManager.mostrarClienteCrearPantallaPanel();
	}

	@Override
	public void executeOnUpdateBttnClicked() {
		ClienteTablaPanel ctp = (ClienteTablaPanel)this.tablePanel;
		int selectedRow = ctp.getCustomersTable().getSelectedRow();
		
		if (selectedRow == -1) {
			JOptionPane.showMessageDialog(tablePanel, "No seleccionaste el registro.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Cliente toUpdate = ctp.getModel().getContent().get(selectedRow);
		
		panelManager.mostrarClienteActualizarPantallaPanel(toUpdate);
		
	}

	@Override
	public void executeOnDeleteBttnClicked() {
		ClienteTablaPanel ctp = (ClienteTablaPanel)this.tablePanel;
		int selectedRow = ctp.getCustomersTable().getSelectedRow();
		
		if (selectedRow == -1) {
			JOptionPane.showMessageDialog(tablePanel, "No seleccionaste el registro.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Cliente toDelete = ctp.getModel().getContent().get(selectedRow);
		
		ClienteService cs = new ClienteService();
		try {
			cs.delete(toDelete.getNombreUsuario());
			
			ctp.getModel().getContent().remove(selectedRow);
			ctp.getModel().fireTableDataChanged();
		} catch (ServicioException e) {
			JOptionPane.showMessageDialog(tablePanel, "No se pudo eliminar el usuario. Detalles: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

}
