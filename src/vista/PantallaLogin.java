package vista;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import controlador.ControladorLogin;
import controlador.ControladorAdminPanelManager;
import exceptions.ServicioException;

public class PantallaLogin extends JFrame implements ActionListener {
	
	JButton entrarBttn;
	JButton exitBttn;
	JTextField txtUsuario;
	JPasswordField txtPassword;
	
	public PantallaLogin() {
		this.setTitle("Home Banking - Login");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(3,2));
		this.setLocationRelativeTo(null);
		
		entrarBttn = new JButton("Entrar");
		exitBttn = new JButton("Salir");
		
		JLabel labelUsuario = new JLabel();
		JLabel labelPassword = new JLabel();
		txtUsuario = new JTextField();
		txtPassword = new JPasswordField();
		labelUsuario.setText("Usuario");
		labelPassword.setText("Contraseña");
		txtUsuario.setPreferredSize(new Dimension(250, 40));
		txtPassword.setPreferredSize(new Dimension(250, 40));
		
		this.add(labelUsuario);
		this.add(txtUsuario);
		this.add(labelPassword);
		this.add(txtPassword);
		this.add(entrarBttn);
		this.add(exitBttn);
		
		this.pack();
		
		exitBttn.addActionListener(this);
		entrarBttn.addActionListener(this);
	}
	
	public void usuarioPassIncorrecto() {
		JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos: ", "Error", JOptionPane.ERROR_MESSAGE);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == exitBttn) {
			ControladorLogin.exitPrograma();
		} else if (e.getSource() == entrarBttn) {
			try {
				ControladorLogin.verificarLogin(txtUsuario.getText(), txtPassword.getText());
			} catch (ServicioException e1) {
				JOptionPane.showMessageDialog(null, "No se pudo verificar: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			} 
		}
	}
}
