package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import controlador.ControladorAdminPanelManager;
import exceptions.ServicioException;

public class PantallaAdministrador implements ActionListener {
	ControladorAdminPanelManager panelManager = null;
	
	public PantallaAdministrador() {
		try {
			panelManager = new ControladorAdminPanelManager();
			panelManager.buildManager();
			panelManager.mostrarHomePanel();
			panelManager.mostrarFrame();
		} catch (ServicioException e2) {
			e2.printStackTrace();
		}
	}
	
	public void usuarioPassIncorrecto() {
		JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos: ", "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

	public void setVisible(boolean b) {
		panelManager.setVisible(b);
	}
}
