package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import exceptions.ServicioException;
import controlador.ControladorAdminPanelManager;

public abstract class EnvioDineroPantallaPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	protected ControladorAdminPanelManager panelManager;
	protected TablaPanel tablePanel;
	protected BotontesPanel crudButtonsPanel;
	
	public EnvioDineroPantallaPanel(ControladorAdminPanelManager panelManager) throws ServicioException {
		this.panelManager= panelManager;
		this.setTablePanel();
		this.setCrudButtonsPanel();
		//buildScreenPanel();
	}
	
	public void buildScreenPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(tablePanel);
		this.add(crudButtonsPanel);
				
		this.crudButtonsPanel.getNewBttn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				executeOnNewBttnClicked();
			}
		});
	
		this.crudButtonsPanel.getUpdateBttn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				executeOnUpdateBttnClicked();
			}
		});
		
		this.crudButtonsPanel.getDeleteBttn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				executeOnDeleteBttnClicked();
			}
		});
	 }
	
	public void setCrudButtonsPanel() {
		this.crudButtonsPanel = new BotontesPanel(this.panelManager);
	}
	
	public abstract void setTablePanel() throws ServicioException;
	public abstract void executeOnNewBttnClicked();
	public abstract void executeOnUpdateBttnClicked();	
	public abstract void executeOnDeleteBttnClicked();

}
