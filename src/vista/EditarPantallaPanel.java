package vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import exceptions.ServicioException;
import controlador.ControladorAdminPanelManager;

public abstract class EditarPantallaPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	protected ControladorAdminPanelManager panelManager;
	protected CampoPanel fieldsPanel;
	protected BotonesPanelNavegador navButtonsPanel;
	
	public EditarPantallaPanel(ControladorAdminPanelManager panelManager) {
		this.panelManager= panelManager;
		this.setFieldsPanel();
		this.setNavButtonsPanel();
		buildScreenPanel();
	}
	
	public void buildScreenPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(fieldsPanel);
		this.add(navButtonsPanel);
		
		this.navButtonsPanel.getSaveBttn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				executeOnSave();
			}
		});
	
		this.navButtonsPanel.getCancelBttn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					executeOnCancel();
				} catch (ServicioException e1) {
					e1.printStackTrace();
				}
			}
		});
	 }
	
	public void setNavButtonsPanel() {
		this.navButtonsPanel = new BotonesPanelNavegador(this.panelManager);
	}
	
	public abstract void setFieldsPanel();
	public abstract void executeOnSave();
	public abstract void executeOnCancel() throws ServicioException;	

}
