package vista;

import javax.swing.JPanel;

import controlador.ControladorAdminPanelManager;

public abstract class TablaPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private ControladorAdminPanelManager panelManager;

	public TablaPanel(ControladorAdminPanelManager panelManager) {
		this.panelManager = panelManager;
		buildForm();
	}
	
	public abstract void buildForm();
}
