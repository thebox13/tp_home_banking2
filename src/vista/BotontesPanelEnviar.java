package vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controlador.ControladorAdminPanelManager;
import controlador.ControladorUsuarioPanelManager;

public class BotontesPanelEnviar extends JPanel {

	private static final long serialVersionUID = 1L;
	private ControladorUsuarioPanelManager panelManager;
	private JButton enviarBttn;
	private JButton homeBttn;
	
	public BotontesPanelEnviar(ControladorUsuarioPanelManager panelManager) {
		this.panelManager = panelManager;
		buildNavButtonsPanel();
	}
	
	public void buildNavButtonsPanel() {
		this.setLayout(new FlowLayout());
		enviarBttn = new JButton("Enviar");
		homeBttn = new JButton("Volver");
		
		this.add(enviarBttn);
		this.add(homeBttn);
		
		this.homeBttn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.mostrarHomePanel();
				
			}
		});
	}

	public JButton getEnviarBttn() {
		return enviarBttn;
	}	
}
