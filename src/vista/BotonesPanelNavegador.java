package vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controlador.ControladorAdminPanelManager;

public class BotonesPanelNavegador extends JPanel {

	private static final long serialVersionUID = 1L;
	private ControladorAdminPanelManager panelManager;
	private JButton saveBttn;
	private JButton cancelBttn;
	private JButton homeBttn;
	
	public BotonesPanelNavegador(ControladorAdminPanelManager panelManager) {
		this.panelManager = panelManager;
		buildNavButtonsPanel();
	}
	
	public void buildNavButtonsPanel() {
		this.setLayout(new FlowLayout());
		saveBttn = new JButton("Guardar");
		cancelBttn = new JButton("Cancelar");
		homeBttn = new JButton("Volver");
		
		this.add(saveBttn);
		this.add(cancelBttn);
		this.add(homeBttn);
		
		this.homeBttn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.mostrarHomePanel();
				
			}
		});
	}

	public JButton getSaveBttn() {
		return saveBttn;
	}

	public JButton getCancelBttn() {
		return cancelBttn;
	}
	
	public JButton getHomeBttn() {
		return homeBttn;
	}	
}
