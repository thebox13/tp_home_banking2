package vista;


import java.awt.FlowLayout;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import data.Cliente;
import exceptions.*;
import controlador.ControladorAdminPanelManager;
import modelo.ClienteTableModel;
import servicio.ClienteService;

public class ClienteTablaPanel extends TablaPanel {
	private static final long serialVersionUID = 1L;
	private JTable customersTable;
	private ClienteTableModel model;

	private JScrollPane scrollPaneForTable;

	public ClienteTablaPanel(ControladorAdminPanelManager panelManager) throws ServicioException {
		super(panelManager);
	}

	@Override
	public void buildForm() {
		this.setLayout(new FlowLayout());

		model = new ClienteTableModel();
		customersTable = new JTable(model);
		this.setSize(800, 800);
		scrollPaneForTable = new JScrollPane(customersTable);
		this.add(scrollPaneForTable);
		
		ClienteService userService = new ClienteService();
		
		try {
			List<Cliente> list = userService.listAll();	
			model.setContent(list);
			model.fireTableDataChanged();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(customersTable, "No se pudieron listar los usuarios.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public JTable getCustomersTable() {
		return customersTable;
	}
	
	public ClienteTableModel getModel() {
		return model;
	}
	
	public void refreshContent() {
		ClienteService userService = new ClienteService();
		
		try {
			List<Cliente> list = userService.listAll();	
			model.setContent(list);
			model.fireTableDataChanged();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(customersTable, "Se ha producido un error al listar usuarios.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
