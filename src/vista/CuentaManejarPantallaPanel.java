package vista;

import javax.swing.JOptionPane;

import data.Cliente;
import data.Cuenta;
import exceptions.ServicioException;
import servicio.ClienteService;
import servicio.CuentaService;
import controlador.ControladorAdminPanelManager;

public class CuentaManejarPantallaPanel extends ManejoPantallaPanel {
	private static final long serialVersionUID = 1L;

	public CuentaManejarPantallaPanel(ControladorAdminPanelManager panelManager) throws ServicioException {
		super(panelManager);
		this.buildScreenPanel();
	}

	@Override
	public void setTablePanel() throws ServicioException {
		this.tablePanel = new CuentaTablaPanel(panelManager);
	}

	@Override
	public void executeOnNewBttnClicked() {
		panelManager.mostrarCuentaCrearPantallaPanel();
	}

	@Override
	public void executeOnUpdateBttnClicked() {
		CuentaTablaPanel ctp = (CuentaTablaPanel)this.tablePanel;
		int selectedRow = ctp.getCuentasTable().getSelectedRow();
		
		if (selectedRow == -1) {
			JOptionPane.showMessageDialog(tablePanel, "No seleccionaste el registro.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Cuenta toUpdate = ctp.getModel().getContent().get(selectedRow);
		
		panelManager.mostrarCuentaActualizarPantallaPanel(toUpdate);
		
	}

	@Override
	public void executeOnDeleteBttnClicked() {
		CuentaTablaPanel ctp = (CuentaTablaPanel)this.tablePanel;
		int selectedRow = ctp.getCuentasTable().getSelectedRow();
		
		if (selectedRow == -1) {
			JOptionPane.showMessageDialog(tablePanel, "No seleccionaste el registro.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Cuenta toDelete = ctp.getModel().getContent().get(selectedRow);
		
		CuentaService cs = new CuentaService();
		try {
			cs.delete(toDelete.getNro_cuenta());
			
			ctp.getModel().getContent().remove(selectedRow);
			ctp.getModel().fireTableDataChanged();
		} catch (ServicioException e) {
			JOptionPane.showMessageDialog(tablePanel, "No se pudo eliminar el usuario. Detalles: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

}
