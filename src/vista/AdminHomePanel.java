package vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import exceptions.ServicioException;
import controlador.ControladorAdminPanelManager;

public class AdminHomePanel extends JPanel{
	private static final long serialVersionUID = 1L;
	protected ControladorAdminPanelManager panelManager;
	private JButton usuariosBttn;
	private JButton agregarCuentaBttn;
	private JButton agregarTarjetaBttn;
	private JButton salirBttn;
	
	public AdminHomePanel(ControladorAdminPanelManager panelManager) {
		this.panelManager = panelManager;
		buildHomePanel();
	}

	public void buildHomePanel() {
		this.setLayout(new FlowLayout());
		this.usuariosBttn = new JButton("Administrar Clientes");
		this.agregarCuentaBttn = new JButton("Administrar Cuenta");
		this.agregarTarjetaBttn = new JButton("Administrar Tarjeta");
		this.salirBttn = new JButton("Salir");
		
		this.add(usuariosBttn);
		this.add(agregarCuentaBttn);
		this.add(agregarTarjetaBttn);
		this.add(salirBttn);
		
		this.usuariosBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					panelManager.mostrarClienteManejarPantallaPanel();
				} catch (ServicioException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		 });
		
		this.agregarCuentaBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					panelManager.mostrarCuentaManejarPantallaPanel();
				} catch (ServicioException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		 });
		
		this.salirBttn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.mostrarExit();
			}
		});
	}
}
