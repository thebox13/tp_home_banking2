package vista;

import javax.swing.JOptionPane;

import data.Cliente;
import data.Cuenta;
import exceptions.ServicioException;
import servicio.ClienteService;
import servicio.CuentaService;
import controlador.ControladorAdminPanelManager;

public class CuentaCrearPantallaPanel extends EditarPantallaPanel {
	private static final long serialVersionUID = 1L;

	public CuentaCrearPantallaPanel(ControladorAdminPanelManager panelManager) {
		super(panelManager);
		//this.buildScreenPanel();
	}

	@Override
	public void setFieldsPanel() {
		this.fieldsPanel = new CuentaCampoPanel(panelManager);

	}

	@Override
	public void executeOnSave() {
		CuentaCampoPanel cfp = (CuentaCampoPanel)this.fieldsPanel;
		CuentaService cs = new CuentaService();
		ClienteService cls = new ClienteService();
		
		try {
			int ultimaCuenta = cs.ultimaCuenta();
			float dinero = Float.valueOf(cfp.getDineroTxt().getText());
			if (cls.findByName(cfp.getUsuarioTxt().getText()) == null) {
				JOptionPane.showMessageDialog(fieldsPanel, "El usuario no existe ", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				Cuenta cuenta = new Cuenta(cfp.getUsuarioTxt().getText(), cfp.getTipoCuentaTxt().getText(), ultimaCuenta + 1, dinero);
				cs.create(cuenta);
				panelManager.mostrarCuentaManejarPantallaPanel();
			}
		} catch (ServicioException e) {
			JOptionPane.showMessageDialog(fieldsPanel, "No se pudo crear la cuenta: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	@Override
	public void executeOnCancel() throws ServicioException {
		panelManager.mostrarCuentaManejarPantallaPanel();
	}
	
	public CuentaCampoPanel getFieldsPanel() {
		return (CuentaCampoPanel)this.fieldsPanel;
	}
}
