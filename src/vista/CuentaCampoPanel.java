package vista;

import java.awt.GridLayout;

import javax.swing.*;

import controlador.ControladorAdminPanelManager;

public class CuentaCampoPanel extends CampoPanel {
	private static final long serialVersionUID = 1L;
	private JTextField usuarioTxt;
	private JTextField nroCuentaTxt;
	private JTextField tipoCuentaTxt;
	private JTextField dineroTxt;
	

	public CuentaCampoPanel(ControladorAdminPanelManager panelManager) {
		super(panelManager);
	}
	
	@Override
	public void buildForm() {
		this.setLayout(new GridLayout(12,2));
		this.setSize(800,800);
		JLabel nombreUsuarioLbl = new JLabel("Usuario:");
		JLabel nroCuentaLbl = new JLabel("Nro Cuenta:");
		JLabel tipoCuentaLbl = new JLabel("Tipo Cuenta:");
		JLabel dineroLbl = new JLabel("Dinero inicial:");
		
		usuarioTxt = new JTextField("");
		nroCuentaTxt = new JTextField("");
		tipoCuentaTxt = new JTextField("");
		dineroTxt = new JTextField("");
		
		this.add(nombreUsuarioLbl);
		this.add(usuarioTxt);
		this.add(nroCuentaLbl);
		this.add(nroCuentaTxt);
		this.add(tipoCuentaLbl);
		this.add(tipoCuentaTxt);
		this.add(dineroLbl);
		this.add(dineroTxt);
	}
	
	public JTextField getUsuarioTxt() {
		return usuarioTxt;
	}

    public void setUsuarioTxt(JTextField usuarioTxt) {
        this.usuarioTxt = usuarioTxt;
    }
    
    public JTextField getTipoCuentaTxt() {
		return tipoCuentaTxt;
	}

    public void setTipoCuentaTxt(JTextField tipoCuentaTxt) {
        this.tipoCuentaTxt = tipoCuentaTxt;
    }
    
    public JTextField getNroCuentaTxt() {
		return nroCuentaTxt;
	}

    public void setNroCuentaTxt(JTextField nroCuentaTxt) {
        this.nroCuentaTxt = nroCuentaTxt;
    }
    
    public JTextField getDineroTxt() {
		return dineroTxt;
	}

    public void setDineroTxt(JTextField dineroTxt) {
        this.dineroTxt = dineroTxt;
    }
}
