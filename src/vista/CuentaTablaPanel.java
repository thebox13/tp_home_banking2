package vista;


import java.awt.FlowLayout;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import data.Cliente;
import data.Cuenta;
import exceptions.*;
import controlador.ControladorAdminPanelManager;
import modelo.ClienteTableModel;
import modelo.CuentaTableModel;
import servicio.ClienteService;
import servicio.CuentaService;

public class CuentaTablaPanel extends TablaPanel {
	private static final long serialVersionUID = 1L;
	private JTable cuentaTable;
	private CuentaTableModel model;

	private JScrollPane scrollPaneForTable;

	public CuentaTablaPanel(ControladorAdminPanelManager panelManager) throws ServicioException {
		super(panelManager);
	}

	@Override
	public void buildForm() {
		this.setLayout(new FlowLayout());

		model = new CuentaTableModel();
		cuentaTable = new JTable(model);
		this.setSize(800, 800);
		scrollPaneForTable = new JScrollPane(cuentaTable);
		this.add(scrollPaneForTable);
		
		CuentaService userService = new CuentaService();
		
		try {
			List<Cuenta> list = userService.listAll();
			model.setContent(list);
			model.fireTableDataChanged();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(cuentaTable, "No se pudieron listar las cuentas.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public JTable getCuentasTable() {
		return cuentaTable;
	}
	
	public CuentaTableModel getModel() {
		return model;
	}
	
	public void refreshContent() {
		CuentaService userService = new CuentaService();
		
		try {
			List<Cuenta> list = userService.listAll();	
			model.setContent(list);
			model.fireTableDataChanged();
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(cuentaTable, "Se ha producido un error al listar cuentas.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
}
