package vista;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import controlador.ControladorAdminPanelManager;

public class BotontesPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private ControladorAdminPanelManager panelManager;
	private JButton newBttn;
	private JButton updateBttn;
	private JButton deleteBttn;
	private JButton homeBttn;
	
	public BotontesPanel(ControladorAdminPanelManager panelManager) {
		this.panelManager = panelManager;
		buildNavButtonsPanel();
	}
	
	public void buildNavButtonsPanel() {
		this.setLayout(new FlowLayout());
		newBttn = new JButton("Nuevo");
		deleteBttn = new JButton("Borrar");
		updateBttn = new JButton("Modificar");
		homeBttn = new JButton("Volver");
		
		this.add(newBttn);
		this.add(deleteBttn);
		this.add(updateBttn);
		this.add(homeBttn);
		
		this.homeBttn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panelManager.mostrarHomePanel();
				
			}
		});
	}

	public JButton getNewBttn() {
		return newBttn;
	}
	
	public JButton getUpdateBttn() {
		return updateBttn;
	}
	
	public JButton getDeleteBttn() {
		return deleteBttn;
	}
	
	public JButton getHomeBttn() {
		return homeBttn;
	}
	
}
