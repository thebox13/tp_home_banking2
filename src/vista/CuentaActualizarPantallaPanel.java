package vista;

import javax.swing.JOptionPane;

import data.Cliente;
import data.Cuenta;
import exceptions.ServicioException;
import servicio.ClienteService;
import servicio.CuentaService;
import controlador.ControladorAdminPanelManager;

public class CuentaActualizarPantallaPanel extends EditarPantallaPanel {
	private static final long serialVersionUID = 1L;

	public CuentaActualizarPantallaPanel(ControladorAdminPanelManager panelManager) {
		super(panelManager);
	}

	@Override
	public void setFieldsPanel() {
		this.fieldsPanel = new CuentaCampoPanel(panelManager);

	}

	@Override
	public void executeOnSave() {
		CuentaCampoPanel cfp = (CuentaCampoPanel)this.fieldsPanel;
		float dinero = Float.valueOf(cfp.getDineroTxt().getText());
		
		Cuenta cuenta = new Cuenta(cfp.getUsuarioTxt().getText(), cfp.getTipoCuentaTxt().getText(), Integer.parseInt(cfp.getNroCuentaTxt().getText()), dinero);
		CuentaService cs = new CuentaService();
		
		try {
			cs.update(cuenta);
			panelManager.mostrarCuentaManejarPantallaPanel();
		} catch (ServicioException e) {
			JOptionPane.showMessageDialog(fieldsPanel, "No se pudo crear la cuenta: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	@Override
	public void executeOnCancel() throws ServicioException {
		panelManager.mostrarClienteManejarPantallaPanel();

	}
	
	public CuentaCampoPanel getFieldsPanel() {
		return (CuentaCampoPanel)this.fieldsPanel;
	}
}
